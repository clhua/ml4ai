package com.ml4ai.test;

import com.google.gson.JsonObject;

public class Version1_1 {

    public static void main() {
        JsonObject target = new JsonObject();
        target.addProperty("familyName", "lee");
        target.addProperty("givenName", "cheng");
        target.keySet().forEach(entry -> {
            System.out.println(entry);
        });
    }

}
