import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EnumPage } from './enum';

@NgModule({
  declarations: [
    EnumPage,
  ],
  imports: [
    IonicPageModule.forChild(EnumPage),
  ],
})
export class BusinessTypePageModule {}
