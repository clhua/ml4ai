import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { NetProvider } from '../../providers/net/net';
import { GlobalsProvider } from '../../providers/globals/globals';
import { LoginPage } from '../login/login';

/**
 * Generated class for the BusinessTypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-enum',
  templateUrl: 'enum.html',
})
export class EnumPage {

  private list: any[] = [];
  private root: string;
  private back: any;
  private title: string = "";

  constructor(public view: ViewController, public navCtrl: NavController, public navParams: NavParams, private net: NetProvider, private globals: GlobalsProvider) {
    this.root = navParams.get("root");
    this.back = navParams.get("callback");
    console.log(this.root, this.back);
    switch (this.root) {
      case "BusinessType":
        this.title = "业务类型";
        break;
    }
    console.log(this.title);
  }

  ionViewDidLoad() {
    let me = this;
    this.net.postJson(this.globals.urls.enums, { enumname: me.root }, (response) => {
      if (response && response.code) {
        if (response.code == "200") {
          me.list = [];
          for (let key in response.data) {
            let name = response.data[key];
            me.list.push({ key: key, name: name });
          }
        } else if (response.code == "401") {
          me.globals.navControl.setRoot(LoginPage);
        }
      }
    }, (error) => {

    });
  }

  select(item) {
    if (this.back) {
      this.back(item.key);
      this.view.dismiss();
    }
  }
}
