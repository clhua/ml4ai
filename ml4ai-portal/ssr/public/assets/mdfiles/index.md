#  ML4AI

### Machine Learning For Artificial Intelligence 机器学习 人工智能

网站：[www.ml4ai.com](http://www.ml4ai.com "www.ml4ai.com")

源代码: https://gitee.com/sleechengn/ml4ai

<p>一个学习大数据、云计算、人工智能、深度学习以及数据科学、信息科学的学习、研究园地，希望你能喜欢，也欢迎大家分享好的文章给我，分享内容支持Markdown格式与HTML格式，请发邮件到：sleechengn@163.com，我将其放上去。</p>

<p>现代人工智能探索已经进入前所未有的旺盛期。近几年来，随首以深度学习为基础的AI实验大量的进行，取得了非常重要的理论和应用成果。现有的深度学习体系，在理论上具备强人工智能的一些基础特征。</p>