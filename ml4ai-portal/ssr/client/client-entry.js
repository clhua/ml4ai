import React from 'react';
import ReactDOM from 'react-dom';
import App from '../pages/app';
import { BrowserRouter } from 'react-router-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from '../ctl/reducer'

const store = createStore(reducer, window && window.INIT_STATE ? JSON.parse(window.INIT_STATE) : {})

const unsub = store.subscribe(() => {
    let state = store.getState()
})

ReactDOM.hydrate(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);