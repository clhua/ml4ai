import React from 'react'
import Markdown from '../fragments/markdown'
import { Menu } from 'antd'
import { Horizontal, West, East, Center, Vertical, South, North, Div } from '../fragments/layout'

import Fragment from '../fragments/fragment'
import map from '../../../../ctl/mapper'
import headImage from '../../../../public/assets/images/head.jpg'

import { Link } from 'react-router-dom'
import MenuComponent from '../fragments/menus'


import PdfViewer from '../fragments/pdf'

const MenuItem = Menu.Item
const SubMenu = Menu.SubMenu
const Component = React.Component

class RootPage extends Component {

    constructor(props) {
        super(props)
    }

    componentWillMount() {
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    content(url) {
        if (typeof url == "string") {
            if (url.endsWith(".md")) {
                return <div className="scrolly width100height100" >
                    <div className="height100 content_lr_100">
                        <div className="padding10 ">
                            <Markdown url={url}></Markdown>
                        </div>
                    </div>
                </div>
            } else if (url.endsWith(".html")) {
                return <div className="scrolly width100height100" >
                    <div className="height100 content_lr_100">
                        <div className="padding10 ">
                            <Fragment url={url}></Fragment>
                        </div>
                    </div>
                </div>
            } else if (url.endsWith(".pdf")) {
                return <div className="scrolly width100height100" >
                    <div className="height100 content_lr_100">
                        <div className="padding10 width100height100">
                            <PdfViewer url={url}></PdfViewer>
                        </div>
                    </div>
                </div>
            } else if (url.startsWith("http://") || url.startsWith("https://")) {
                return <div className="overflowHidden width100height100" >
                    <iframe src={url} style={{ width: "100%", height: "100%", border: "none" }}></iframe>
                </div>
            }
        } else {
            let component = url
            return component
        }
    }

    render() {
        return (
            <Vertical>
                <North height={48}>
                    <Div style={{ backgroundColor: "#e7e7e7", width: "100%", height: "100%" }}>
                        <div className="content_lr_100 root_menu">
                            <Horizontal>
                                <West width={48}>
                                    <img width="48" height="48" src={headImage}></img>
                                </West>
                                <Center>
                                    <MenuComponent />
                                </Center>
                            </Horizontal>
                        </div>
                    </Div>
                </North>
                <Center>
                    <Div className="width100height100" style={{ backgroundImage: this.props.background ? this.props.background : "url(/assets/images/ai/ai-background.jpg)", backgroundSize: "cover" }}>
                        {this.content(this.props.routePage)}
                    </Div>
                </Center>
                <South style={{ backgroundColor: "#e7e7e7" }} height={120}>
                    <div className="content_lr_100">
                        <div className="padding10">
                            <Markdown url="/assets/mdfiles/content/content-foot.md"></Markdown>
                        </div>
                    </div>
                </South>
            </Vertical>
        )
    }
}

export default map(RootPage)

