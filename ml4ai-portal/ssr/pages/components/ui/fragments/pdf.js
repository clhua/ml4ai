import React from 'react'
import map from '../../../../ctl/mapper'

class PdfViewer extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
    }

    render() {
        return <div className="width100height100">
            <iframe style={{ width: "100%", height: "100%", border: "none" }} src={`/assets/pdfjs/web/viewer.html?file=${this.props.url}`}></iframe>
        </div>
    }

}

export default map(PdfViewer)

