import React, { Component } from 'react';
import './App.css';
import { WelCome } from './pages/management/welcome';
import { Login } from './pages/auth/auth';
import { global } from './config.service';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            page: <WelCome />
        };
        global.root = this;
    }

    componentDidMount() {

    }

    render() {
        return this.state.page;
    }
}

export default App;
