import React from 'react';
import { Form, Icon, Input, Modal, Button } from 'antd';
import { ajax, api, global } from '../../config.service';
import { WelCome } from '../management/welcome'

const FormItem = Form.Item;

class Login extends React.Component {

    onSubmit(me) {
        let data = me.loginForm.getFieldsValue();
        console.log(data);
        ajax.post(api.login, data, (response) => {
            global.root.setState({
                page: <WelCome />
            });
        });
    }

    constructor(props) {
        super(props);
    }

    render() {
        const UserForm = Form.create({})(LoginForm);
        return (
            <div>
                <Modal visible={true} footer={null} onCancel={() => { window.close(); }}>
                    <UserForm onSubmit={() => { this.onSubmit(this); }} registry={(subComponent) => { this.loginForm = subComponent; }} />
                </Modal>
            </div>
        );
    }
}

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        if (props.registry) {
            props.registry(this);
        }
    }
    getFieldsValue() {
        return this.props.form.getFieldsValue();
    }
    componentWillReceiveProps(nextProps) {
        this.props = nextProps;
    }
    init() {
    }
    setFieldsValue(fieldsValue) {
        this.props.form.setFieldsValue(fieldsValue);
    }
    componentDidMount() {
        this.init();
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form>
                <FormItem label="账号">
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: '请输入用户' }],
                    })(
                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="用户" />
                    )}
                </FormItem>
                <FormItem label="密码">
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: '请输入密码' }],
                    })(
                        <Input type="password" prefix={< Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="密码" />
                    )}
                </FormItem>
                <FormItem>
                    <Button onClick={this.props.onSubmit} type="primary">登录</Button>
                </FormItem>
            </Form>
        );
    }

}

export { Login };