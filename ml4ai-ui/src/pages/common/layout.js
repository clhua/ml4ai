import React from 'react';

const Component = React.Component;

class Horizontal extends Component {

    render() {
        let childrenLayout = [];
        let centerLeft = 0;
        let centerRight = 0;
        let centerTop = 0;
        let centerBottom = 0;
        React.Children.map(this.props.children, (component) => {
            if (component.type == Center) {
            } else if (component.type == West) {
                centerLeft = component.props.width;
            } else if (component.type == East) {
                centerRight = component.props.width;
            } else {
                throw "不支持的类型：" + component.toString();
            }
        });
        React.Children.map(this.props.children, (component) => {
            if (component.type == Center) {
                let top = centerTop + "px";
                let bottom = centerBottom + "px";
                let left = centerLeft + "px";
                let right = centerRight + "px";
                childrenLayout.push(<Center properties={component.props.properties ? component.props.properties : undefined} style={component.props.style ? component.props.style : {}} top={top} bottom={bottom} right={right} left={left}>{component.props.children}</Center>);
            } else if (component.type == West) {
                let width = component.props.width + "px";
                childrenLayout.push(<West properties={component.props.properties ? component.props.properties : undefined} style={component.props.style ? component.props.style : {}} width={width}>{component.props.children}</West>);
            } else if (component.type == East) {
                let width = component.props.width + "px";
                childrenLayout.push(<East properties={component.props.properties ? component.props.properties : undefined} style={component.props.style ? component.props.style : {}} width={width}>{component.props.children}</East>);
            }
        });
        return (
            <div style={{ width: "100%", height: "100%", display: "block", position: "relative" }}>
                {childrenLayout}
            </div>
        );
    }

}

class Vertical extends Component {
    render() {
        let childrenLayout = [];
        let centerLeft = 0;
        let centerRight = 0;
        let centerTop = 0;
        let centerBottom = 0;
        React.Children.map(this.props.children, (component) => {
            if (component.type == Center) {

            } else if (component.type == South) {
                centerBottom = component.props.height;
            } else if (component.type == North) {
                centerTop = component.props.height;
            } else {
                throw "不支持的类型：" + component.toString();
            }
        });
        React.Children.map(this.props.children, (component) => {
            if (component.type == Center) {
                let top = centerTop + "px";
                let bottom = centerBottom + "px";
                let left = centerLeft + "px";
                let right = centerRight + "px";
                childrenLayout.push(<Center properties={component.props.properties ? component.props.properties : undefined} style={component.props.style ? component.props.style : {}} top={top} bottom={bottom} right={right} left={left}>{component.props.children}</Center>);
            } else if (component.type == South) {
                let height = component.props.height + "px";
                childrenLayout.push(<South properties={component.props.properties ? component.props.properties : undefined} style={component.props.style ? component.props.style : {}} height={height}>{component.props.children}</South>);
            } else if (component.type == North) {
                let height = component.props.height + "px";
                childrenLayout.push(<North properties={component.props.properties ? component.props.properties : undefined} style={component.props.style ? component.props.style : {}} height={height}>{component.props.children}</North>);
            }
        });
        return (
            <div style={{ width: "100%", height: "100%", display: "block", position: "relative" }}>
                {childrenLayout}
            </div>
        );
    }
}

class West extends Component {

    render() {
        return (
            <div {...(this.props.properties ? this.props.properties : undefined)} style={{ ...(this.props.style ? this.props.style : undefined), width: this.props.width, position: "absolute", left: "0px", top: "0px", bottom: "0px" }}>
                {this.props.children}
            </div>
        );
    }

}

class South extends Component {
    render() {
        return (
            <div {...(this.props.properties ? this.props.properties : undefined)} style={{ ...(this.props.style ? this.props.style : undefined), height: this.props.height, position: "absolute", left: "0px", right: "0px", bottom: "0px" }}>
                {this.props.children}
            </div>
        );
    }
}

class North extends Component {
    render() {
        return (
            <div {...(this.props.properties ? this.props.properties : undefined)} style={{ ...(this.props.style ? this.props.style : undefined), height: this.props.height, position: "absolute", left: "0px", right: "0px", top: "0px" }}>
                {this.props.children}
            </div>
        );
    }
}

class East extends Component {
    render() {
        return (
            <div {...(this.props.properties ? this.props.properties : undefined)} style={{ ...(this.props.style ? this.props.style : undefined), width: this.props.width, position: "absolute", right: "0px", top: "0px", bottom: "0px" }}>
                {this.props.children}
            </div>
        );
    }
}

class Center extends Component {
    render() {
        return (
            <div style={{ position: "absolute", left: this.props.left, right: this.props.right, top: this.props.top, bottom: this.props.bottom }}>
                {this.props.children}
            </div>
        );
    }
}

class Div extends Component {

    render() {
        return (<div {...(this.props.properties ? this.props.properties : {})} style={{ ...(this.props.style ? this.props.style : {}) }}>{this.props.children}</div>);
    }

}

export { Horizontal, East, West, Center, Vertical, South, North, Div }