package com.ml4ai.core.test.vo;

public class TVO {

    private static String value;

    public static void setValue(String value) {
        TVO.value = value;
    }

    public static String getValue() {
        return value;
    }
}
