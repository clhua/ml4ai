package com.ml4ai.rabbitmq;

import com.rabbitmq.client.*;
import lombok.Builder;
import lombok.Data;
import lombok.SneakyThrows;
import scala.Tuple2;

import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by leecheng on 2018/11/14.
 */
@Builder
@Data
public class RabbitMQ implements Closeable {

    private String host;

    private Integer port;

    private String vHost;

    private String user;

    private String password;

    private ConnectionFactory connectionFactory;

    private Connection connection;

    private Channel commandChannel;

    @SneakyThrows
    public void init() {
        if (connectionFactory == null) {
            connectionFactory = new ConnectionFactory();
            connectionFactory.setVirtualHost(vHost);
            connectionFactory.setHost(host);
            connectionFactory.setPort(port);
            connectionFactory.setUsername(user);
            connectionFactory.setPassword(password);
        }
        if (!(connection != null && connection.isOpen())) {
            connection = connectionFactory.newConnection();
        }
    }

    @SneakyThrows
    public Channel getWorkChannel() {
        if (connection == null || !connection.isOpen()) {
            init();
        }
        return connection.createChannel();
    }

    @SneakyThrows
    public Channel getCommandChannel() {
        if (commandChannel == null || !commandChannel.isOpen() || connection == null || !connection.isOpen()) {
            init();
            commandChannel = connection.createChannel();
        }
        return commandChannel;
    }

    @SneakyThrows
    public void declareExchange(String exchange, BuiltinExchangeType exchangeType, boolean persistent) {
        getCommandChannel().exchangeDeclare(exchange, exchangeType, persistent);
    }

    @SneakyThrows
    public void declareQueue(String queue, boolean persistent, boolean exclusive, boolean autoDelete, Map<String, Object> parameter) {
        getCommandChannel().queueDeclare(queue, persistent, exclusive, autoDelete, parameter);
    }

    @SneakyThrows
    public void bindQueue(String queue, String exchange, String routeKey, Map<String, Object> parameter) {
        getCommandChannel().queueBind(queue, exchange, routeKey, parameter);
    }

    @SneakyThrows
    public void bindExchange(String target, String root, String rootKey, Map<String, Object> parameter) {
        getCommandChannel().exchangeBind(target, root, rootKey, parameter);
    }

    @SneakyThrows
    public void deleteQueue(String queue) {
        getCommandChannel().queueDelete(queue);
    }

    @SneakyThrows
    public void deleteExchange(String... a) {
        for (String e : a) {
            getCommandChannel().exchangeDelete(e);
        }
    }

    @SneakyThrows
    public void produceText(String exchange, String routeKey, String message, boolean persistent) {
        produce(exchange, routeKey, message.getBytes("UTF-8"), persistent);
    }

    @SneakyThrows
    public void produce(String exchange, String routeKey, byte[] message, boolean persistent) {
        Channel channel = getWorkChannel();
        channel.txSelect();
        channel.basicPublish(exchange, routeKey, persistent ? MessageProperties.PERSISTENT_BASIC : MessageProperties.BASIC, message);
        channel.txCommit();
        channel.abort();
    }

    public Tuple2<String, Channel> consumeText(String queue, int count, Function<String, Boolean> consumerOperation) {
        return consume(queue, count, bytes -> {
            try {
                return consumerOperation.apply(new String(bytes, "UTF-8"));
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    public Tuple2<String, Channel> attachInterimConsumeText(int count, Function<String, Boolean> consumerOperation) {
        return attachInterimConsume(count,
                bytes -> {
                    try {
                        return consumerOperation.apply(new String(bytes, "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        throw new RuntimeException(e);
                    }
                }
        );
    }

    public boolean isQueueExists(String queue) {
        try {
            getCommandChannel().queueDeclarePassive(queue);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @SneakyThrows
    public Tuple2<String, Channel> consume(String queue, int count, Function<byte[], Boolean> consumerOperation) {
        Channel channel = getWorkChannel();
        try {
            return createConsumer(queue, count, channel, consumerOperation);
        } catch (Exception e) {
            channel.close();
            throw new IllegalStateException(e);
        }
    }

    @SneakyThrows
    public Tuple2<String, Channel> attachInterimConsume(int count, Function<byte[], Boolean> consumerOperation) {
        Channel channel = getWorkChannel();
        try {
            String queue = channel.queueDeclare().getQueue();
            return createConsumer(queue, count, channel, consumerOperation);
        } catch (Exception e) {
            channel.close();
            throw new IllegalStateException(e);
        }
    }

    private Tuple2<String, Channel> createConsumer(String queue, int count, Channel channel, Function<byte[], Boolean> consumerOperatition) throws IOException {
        channel.basicQos(count);
        Consumer consumer = createConsumer(channel, consumerOperatition);
        channel.basicConsume(queue, false, consumer);
        return new Tuple2<>(queue, channel);
    }

    private Consumer createConsumer(Channel channel, Function<byte[], Boolean> consumerOperation) {
        return new DefaultConsumer(channel) {
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                try {
                    boolean ack = consumerOperation.apply(body);
                    if (ack) {
                        channel.basicAck(envelope.getDeliveryTag(), false);
                    } else {
                        channel.basicReject(envelope.getDeliveryTag(), true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @SneakyThrows
    public void close() {
        if (connection != null && connection.isOpen()) {
            connection.close();
        }
    }

}
