package com.ml4ai.server.services.impl;

import com.ml4ai.server.services.RabbitmqService;
import com.ml4ai.rabbitmq.RabbitMQ;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class RabbitmqServiceImpl implements RabbitmqService {

    RabbitMQ rabbitMQAgent;

    @Value("${message.queue.host}")
    private String rabbitmqHost;

    @Value("${message.queue.port}")
    private Integer rabbitmqPort;

    @Value("${message.queue.username}")
    private String rabbitmqUsername;

    @Value("${message.queue.password}")
    private String rabbitmqPassword;

    @Value("${message.queue.virtualHost}")
    private String rabbitmqVirtualHost;

    @PostConstruct
    public void initialize(){
        rabbitMQAgent = RabbitMQ.builder().user(rabbitmqUsername).password(rabbitmqPassword).host(rabbitmqHost).port(rabbitmqPort).vHost(rabbitmqVirtualHost).build();
    }

    @Override
    public RabbitMQ getRabbitMQAgent() {
        return rabbitMQAgent;
    }
}
