package com.ml4ai.server.services;

import com.ml4ai.server.domain.Text;
import com.ml4ai.server.dto.TextDTO;
import com.ml4ai.server.services.base.BaseService;

/**
 * Created by leecheng on 2018/10/28.
 */
public interface TextService extends BaseService<Text, TextDTO> {
}
