package com.ml4ai.server.services;

import com.ml4ai.server.domain.CrawlTask;
import com.ml4ai.server.dto.CrawlTaskDTO;
import com.ml4ai.server.services.base.BaseService;

/**
 * Created by leecheng on 2018/10/28.
 */
public interface CrawlTaskService extends BaseService<CrawlTask, CrawlTaskDTO> {
}
