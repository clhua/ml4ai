package com.ml4ai.server.services.impl;

import com.ml4ai.server.domain.IntraCityDistributionTask;
import com.ml4ai.server.dto.IntraCityDistributionTaskDTO;
import com.ml4ai.server.repository.IntraCityDistributionTaskRepo;
import com.ml4ai.server.services.IntraCityDistributionTaskService;
import com.ml4ai.server.services.base.impl.BaseServiceImpl;
import com.ml4ai.server.services.mappers.IntraCityDistributionTaskMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Function;

/**
 * Created by uesr on 2018/9/12.
 */
@Service
@Transactional
public class IntraCityDistributionTaskServiceImpl extends BaseServiceImpl<IntraCityDistributionTask, IntraCityDistributionTaskDTO> implements IntraCityDistributionTaskService {

    @Autowired
    IntraCityDistributionTaskRepo intraCityDistributionTaskRepo;

    @Autowired
    IntraCityDistributionTaskMapper intraCityDistributionTaskMapper;

    @Override
    public Function<IntraCityDistributionTask, IntraCityDistributionTaskDTO> getConvertEntity2DTOFunction() {
        return intraCityDistributionTaskMapper::e2d;
    }

    @Override
    public Function<IntraCityDistributionTaskDTO, IntraCityDistributionTask> getConvertDTO2EntityFunction() {
        return intraCityDistributionTaskMapper::d2e;
    }

    @Override
    public JpaRepository<IntraCityDistributionTask, Long> getRepository() {
        return intraCityDistributionTaskRepo;
    }
}
