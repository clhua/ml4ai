package com.ml4ai.server.services;

import com.ml4ai.server.domain.Role;
import com.ml4ai.server.dto.RoleDTO;
import com.ml4ai.server.services.base.BaseService;

import java.util.List;

/**
 * Created by leecheng on 2018/9/24.
 */
public interface RoleService extends BaseService<Role, RoleDTO> {

    List<RoleDTO> findByUserId(Long userId);

}
