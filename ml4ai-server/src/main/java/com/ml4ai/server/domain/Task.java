package com.ml4ai.server.domain;

import com.ml4ai.server.domain.base.BaseAuditEntity;
import com.ml4ai.server.domain.base.BusinessType;
import com.ml4ai.server.domain.base.ProcessStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/9.
 */
@Setter
@Getter
@Entity
@Table(name = "T_TASK")
@Inheritance(strategy = InheritanceType.JOINED)
public class Task extends BaseAuditEntity {

    //业务编号
    @Column
    private String businessCode;

    //业务类型
    @Enumerated(EnumType.STRING)
    @Column
    private BusinessType businessType;

    //业务标题
    @Column
    private String businessTitle;

    @Column
    private Double amount;

    //业务发布坐标经度
    @Column
    private Double locationX;

    //业务发布坐标纬度
    @Column
    private Double locationY;

    //业务发布人
    @ManyToOne
    @JoinColumn(name = "c_start")
    private User starter;

    //业务发布时间
    @Column
    private Long startTime;

    //业务持有人
    @ManyToOne
    @JoinColumn(name = "c_take")
    private User taker;

    //业务接单时间
    @Column
    private Long takeTime;

    //业务状态
    @Enumerated(EnumType.STRING)
    @Column
    private ProcessStatus processStatus;

    //业务站点
    @ManyToOne
    @JoinColumn(name = "c_site")
    private Site site;
}
