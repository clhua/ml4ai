package com.ml4ai.server.domain;

import com.ml4ai.server.domain.base.BaseAuditEntity;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/16.
 */
@Data
@Entity
@Table(name = "T_TICKET")
public class Ticket extends BaseAuditEntity {

    @Column
    private String ticket;

    @ManyToOne
    @JoinColumn(name = "c_user")
    private User user;

}
