package com.ml4ai.server.domain;

import com.ml4ai.server.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;

/**
 * Created by uesr on 2018/9/9.
 */
@Getter
@Setter
@Entity
@Table(name = "T_AUTH")
public class Authority extends BaseAuditEntity implements GrantedAuthority {

    @Column
    private String authority;

    @Column
    private String authorityName;

    @Column
    private String authorityData;

    @ManyToMany
    @JoinTable(name = "T_ROLE_AUTH", joinColumns = @JoinColumn(name = "C_AUTH"), inverseJoinColumns = @JoinColumn(name = "C_ROLE"))
    private List<Role> roles;

    @ManyToMany
    @JoinTable(name = "T_AUTH_SRC", joinColumns = @JoinColumn(name = "C_AUTH"), inverseJoinColumns = @JoinColumn(name = "C_SRC"))
    private List<Resource> resources;

}
