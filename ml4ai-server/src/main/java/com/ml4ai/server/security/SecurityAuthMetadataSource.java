package com.ml4ai.server.security;


import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;

public interface SecurityAuthMetadataSource extends
        FilterInvocationSecurityMetadataSource {

    void loadResource();

}