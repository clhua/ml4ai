package com.ml4ai.server.repository;

import com.ml4ai.server.domain.CrawlTask;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by leecheng on 2018/10/28.
 */
public interface CrawlTaskRepo extends JpaRepository<CrawlTask, Long> {
}
