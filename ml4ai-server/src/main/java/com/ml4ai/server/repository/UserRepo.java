package com.ml4ai.server.repository;

import com.ml4ai.server.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uesr on 2018/9/8.
 */
public interface UserRepo extends JpaRepository<User, Long> {

    User findByLoginAndStatus(String login, String status);

}
