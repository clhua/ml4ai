package com.ml4ai.server.repository;

import com.ml4ai.server.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uesr on 2018/9/9.
 */
public interface TaskRepo extends JpaRepository<Task, Long> {

    Task findByBusinessCode(String businessCode);

}
