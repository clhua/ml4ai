package com.ml4ai.server;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.ml4ai.server")
@EnableJpaRepositories(basePackages = {"com.ml4ai.server"})
@EnableJpaAuditing
public class Boot {

    static {
        System.setProperty("es.set.netty.runtime.available.processors", "false");
    }

}
