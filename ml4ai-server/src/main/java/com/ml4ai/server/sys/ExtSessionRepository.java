package com.ml4ai.server.sys;

import com.ml4ai.server.services.CacheService;
import com.ml4ai.server.utils.ObjUtil;
import com.ml4ai.server.utils.StringHelper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.Session;
import org.springframework.session.SessionRepository;
import org.springframework.stereotype.Component;

/**
 * Created by leecheng on 2017/11/9.
 */
@Component
public class ExtSessionRepository implements SessionRepository<Session> {

    @Autowired
    private CacheService cacheService;

    @Override
    public ExtSession createSession() {
        String sessionId = StringHelper.uuid();
        return new ExtSession(sessionId, 300);
    }

    @Override
    @SneakyThrows
    public void save(Session session) {
        String val = ObjUtil.obj2str(session);
        cacheService.put("SESSION_STORE", session.getId(), val);
    }

    @Override
    @SneakyThrows
    public ExtSession findById(String id) {
        try {
            String val = (String) cacheService.get("SESSION_STORE", id);
            if (val == null) {
                return null;
            } else {
                return (ExtSession) ObjUtil.str2obj(val);
            }
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void deleteById(String id) {
        cacheService.delete("SESSION_STORE", id);
    }


}