package com.ml4ai.server.sys;

import com.ml4ai.server.utils.StringHelper;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.session.Session;

import java.io.Serializable;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Set;

/**
 * Created by leecheng on 2017/11/9.
 */
@Getter
@Setter
@ToString
public class ExtSession implements Session, Serializable {

    String id;
    int maxInactiveIntervalInSeconds;
    long createTime = Clock.systemUTC().millis();
    long lastAccessedTime = Clock.systemUTC().millis();
    Map<String, Object> value = new java.util.HashMap<>();

    public ExtSession(String id, int maxInactiveIntervalInSeconds) {
        this.id = id;
        this.maxInactiveIntervalInSeconds = maxInactiveIntervalInSeconds;
    }

    public Object getAttribute(String attributeName) {
        Object valueDetail = value.get(attributeName);
        lastAccessedTime = Clock.systemUTC().millis();
        return valueDetail;
    }

    public Set<String> getAttributeNames() {
        Set<String> keySet = value.keySet();
        lastAccessedTime = Clock.systemUTC().millis();
        return keySet;
    }

    public Instant getCreationTime() {
        return Instant.ofEpochMilli(createTime);
    }

    public Instant getLastAccessedTime() {
        return Instant.ofEpochMilli(lastAccessedTime);
    }

    public String getId() {
        return id;
    }

    public boolean isExpired() {
        return Clock.systemUTC().millis() - getLastAccessedTime().toEpochMilli() < getMaxInactiveInterval().getSeconds() * 1000L;
    }

    public void removeAttribute(String attributeName) {
        value.remove(attributeName);
        lastAccessedTime = Clock.systemUTC().millis();
    }

    public void setAttribute(String attributeName, Object attributeValue) {
        lastAccessedTime = Clock.systemUTC().millis();
        value.put(attributeName, attributeValue);
    }

    public String changeSessionId() {
        return StringHelper.uuid();
    }

    public Duration getMaxInactiveInterval() {
        return Duration.ofSeconds(maxInactiveIntervalInSeconds);
    }

    public void setLastAccessedTime(Instant lastAccessedTime) {
        this.lastAccessedTime = Clock.systemUTC().millis();
    }

    public void setMaxInactiveInterval(Duration interval) {
        this.maxInactiveIntervalInSeconds = (int) interval.getSeconds();
    }
}
