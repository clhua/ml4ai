package com.ml4ai.server.services.providers

import akka.actor.ActorSystem
import com.ml4ai.server.services.RabbitmqService
import com.ml4ai.server.services.actors.RootActors
import com.ml4ai.server.services.actors.anys.FileStreamTaskStop
import com.ml4ai.server.services.capability._
import com.ml4ai.rabbitmq.RabbitMQ
import javax.annotation.PostConstruct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class CloseMessageCommandReceiver extends CommandReceiver {

  @Autowired var rabbitmqService: RabbitmqService = null

  var rabbitMQAgent: RabbitMQ = null

  @PostConstruct
  def init(): Unit = {
    rabbitMQAgent = rabbitmqService.getRabbitMQAgent
  }

  override def getSupport(): String = "remove.queue"

  override def execute(command: Command): Unit = {
    val queue = command.get("queue")
    RootActors.root.actorSelection(RootActors.root / "file2queue") ! FileStreamTaskStop(queue)
    println(s"关闭任务：${queue}")
  }

}
