package com.ml4ai.server.services.providers

import com.google.gson.Gson
import com.ml4ai.server.consts.UserConstants
import com.ml4ai.server.services.{DbMapService, RabbitmqService, UserService}
import com.ml4ai.server.services.capability._
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import com.ml4ai.gson._

@Service
class GetUserMessageReceiver extends CommandReceiver {

  @Autowired
  var dbMapService: DbMapService = null

  @Autowired
  var message: RabbitmqService = null

  @Autowired
  var userService: UserService = null

  override def execute(command: Command): Unit = {
    val json = new Gson
    val token = command.get("token")
    val tokenStoreMap = dbMapService.generateLocalMapWrapper(UserConstants.TOKEN_USER_MAP)
    val user = tokenStoreMap.get(token)
    if (user != null) {
      var msg = "{}"
      msg = msg.addString("code", "200")
      msg = msg.addString("message", "获取成功")
      msg = msg.addString("data", json.toJson(userService.findByLogin(user)))
      message.getRabbitMQAgent.produceText("", command.getCallback(), msg, false)
    } else {
      var msg = "{}"
      msg = msg.addString("code", "401")
      msg = msg.addString("message", "未登录")
      message.getRabbitMQAgent.produceText("", command.getCallback(), msg, false)
    }
  }

  override def getSupport(): String = "getUserInfo"

}
