package com.ml4ai.server.services.capability

trait CommandReceiver {

  def getSupport(): String

  def execute(command: Command): Unit

}